const body = document.querySelector('body');
const boxShadow = document.querySelector('.box-shadow');

const sidebar = document.querySelector('.sidebar');
const sidebareClose = document.querySelector('.js-sidebareClose');

const tableButton = document.querySelectorAll('.js-tableBtn');
const chartBtn = document.querySelectorAll('.js-chartBtn');

// Table in table
tableButton.forEach(function(btn) {
  btn.addEventListener('click', function() {
    this.classList.toggle('table__button--active');
    this.closest('.table__row').nextElementSibling.classList.toggle('table__cover--show');
  })
})

//PopUp
let popUp = document.createElement('div');
const createPopUp = function() {
    popUp.classList.add('popUp');
    popUp.innerHTML = `
    <div class='popUp__wrap'>
        <h2 class='popUp__caption'>Table 2 details</h2>
        <a class='popUp__close  js-pupUpClose' href='#'></a>
    </div>
    <div class='popUp__wrapper'>
    <table class='popUp__table'>
        <tr>
            <td>
                <h3>Claim Number</h3>
                <p>2015274101</p>
            </td>
            <td>
                <h3>Billing Provider Name</h3>
                <p>Dun Rite Lawn Maintenanc</p>
            </td>
            <td>
                <h3>Billing Provider Address</h3>
                <p>4343 Columbia Mine Road, 455</p>
            </td>
            <td>
                <h3>Billing Provider Phone</h3>
                <p>9049980211</p>
            </td>
        </tr>
        <tr>
            <td>
                <h3>Claimant Name</h3>
                <p>Beth Towns</p>
            </td>
            <td>
                <h3>Claimant’s SSN</h3>
                <p>426490507</p>
            </td>
            <td>
                <h3>Claimant DOB</h3>
                <p>12/31/1969</p>
            </td>
            <td>
                <h3>Claimant Gender</h3>
                <p>Female</p>
            </td>
        </tr>
        <tr>
            <td>
                <h3>Claimant Address</h3>
                <p>474 Mcwhorter Road, Mem</p>
            </td>
            <td>
                <h3>Insured’s Policy / Group Number</h3>
                <p>157051802</p>
            </td>
            <td>
                <h3>Diagnosis Or Nature of Injury</h3>
                <p>Fracture</p>
            </td>
            <td>
                <h3>Date of Injury</h3>
                <p>08/16/2015</p>
            </td>
        </tr>
    </table>
    </div>

    <div class='popUp__wrapper'>
    <table class='popUp__chart'>
        <thead>
            <tr>
                <td>#</td>
                <td>service from da..</td>
                <td>service through..</td>
                <td>Billed Procedur..</td>
                <td>paid Procedure ...</td>
                <td>paid Procedure Code desk</td>
                <td>Line Charge</td>
                <td>Line allowed..</td>
                <td>place of...</td>
                <td>place of...</td>
                <td>modifier</td>
                <td>units..</td>
                <td>Rendering...</td>
            </tr>
        </thead>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        <tr>
            <td>1</td>
            <td>04-12-2018</td>
            <td>04-12-2018</td>
            <td>0000</td>
            <td>97110</td>
            <td>MANUAL THERAPY TQS 1/> RE</td>
            <td>$95.00</td>
            <td>$195.08</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td>11</td>
            <td></td>
        </tr>

        
    </table>    
    </div>
    `;
    document.body.append(popUp);
    const pupUpClose = document.querySelector('.js-pupUpClose');
    pupUpClose.addEventListener('click', function() {
        boxShadow.classList.remove('box-shadow--active');
        popUp.remove();
        body.classList.remove('overflow');
    });
}

chartBtn.forEach(function(btn) {
  btn.addEventListener('click', function() {
    createPopUp();
    boxShadow.classList.add('box-shadow--active');
    body.classList.add('overflow');
  })
});

//BoxShadow
boxShadow.addEventListener('click', function() {
    this.classList.remove('box-shadow--active');
    popUp.remove();
    sidebar.classList.remove('sidebar--active');
    sidebareClose.classList.remove('sidebar__close--open');
    body.classList.remove('overflow');
});

//Sidebar
sidebareClose.addEventListener('click', function() {
    this.classList.toggle('sidebar__close--open');
    boxShadow.classList.toggle('box-shadow--active');
    sidebar.classList.toggle('sidebar--active');
    body.classList.toggle('overflow');
});